/*
 * No explanation.
 */

this.on('start', function() {
  this.thrusters.top(true);
});

this.on('sensor:bottom', function(status) {
  if (status) {
    this.thrusters.top(false);
    this.thrusters.left(true);
  }
});

this.on('sensor:right', function(status) {
  if (status) {
    this.thrusters.left(false);
    this.thrusters.bottom(true);
  }
});

this.on('sensor:top', function(status) {
  if (status) {
    this.thrusters.bottom(false);
    this.thrusters.right(true);
  }
});

this.on('sensor:left', function(status) {
  if (status) {
    this.thrusters.right(false);
    this.thrusters.top(true);
  }
});
