/*
 * Open all three doors to exit.
 *
 * The answer is ?.
 */

// randomly throw switches until the door is open

var points = {
  switch1: {
    x: 200,
    y: 400
  },
  switch2: {
    x: 300,
    y: 400
  },
  switch3: {
    x: 390, // wasn't activating switch at 400
    y: 400
  },
  home: {
    x: 300,
    y: 300
  },
  exit: {
    x: 300,
    y: 0
  },
  start: {
    x: 50,
    y: 200
  },
};

var goingfrom = 'start';
var goingto = 'home';
var whatsup='inity';
var lastswitch='none';
var refx = 0;
var refy = 0;

// move x-then-y when moving from home to a switch and vice-versa so
// the wrong switch isn't tripped
function whatsnext(bot) {
  if (goingto.substr(0,6) == 'switch') {
    whatsup = 'inity';
    goingfrom = goingto;
    goingto = 'home';
    console.log('heading home');
  } else if (goingto == 'home') {
    whatsup = 'canigo';
    goingfrom = goingto;
    goingto = 'exit';
    console.log('can I go?');
  } else if (goingto == 'exit') {
    whatsup = 'initx';
    do {
    	goingto = 'switch'+Math.ceil(Math.random()*3);
    } while (goingto == lastswitch); // going back to the last switch is kinda silly
    lastswitch = goingto;
    console.log('rolling the dice... heading to '+goingto);
  } else {
    console.log('I shouldn\'t be here. Here\'s whatsup: '+whatsup+
        ' and where I\'m going: '+goingto);
  }
  bot.radar.ping();
}

this.on('start', function() {
  // start taking soundings
  this.radar.angle(270);
  this.radar.ping();
});

this.on('radar:hit', function(angle,distance) {
  switch(whatsup) {
    case 'inity':
      // prepare to move in the y-direction
      if (angle == 270) {
        refy = distance;
        whatsup = 'movey';
        if (points[goingfrom].y < points[goingto].y-5) {
          this.thrusters.bottom(true);
        } else if (points[goingfrom].y > points[goingto].y+5) {
          this.thrusters.top(true);
        } 
      } else {
        this.radar.angle(270);
      }
      break;
    case 'initx':
      // prepare to move in the x-direction
      if (angle == 180) {
        refx = distance;
        whatsup = 'movex';
        if (points[goingfrom].x < points[goingto].x-5) {
          this.thrusters.left(true);
        } else if (points[goingfrom].x > points[goingto].x+5) {
          this.thrusters.right(true);
        }
      } else {
        this.radar.angle(180);
      }
      break;
    case 'movey':
      // actively moving in y-direction
      if (angle == 270) {
        if (Math.abs((refy - distance) - (points[goingto].y - points[goingfrom].y)) < 5) {
          this.thrusters.top(false);
          this.thrusters.bottom(false);
          if (goingto.substr(0,6) == 'switch') {
            whatsnext(this);
          } else {
            whatsup = 'initx';
          }
        }
      } else {
        this.radar.angle(270);
      }
      break;
    case 'movex':
      // actively moving in x-direction
      if (angle == 180) {
        if (Math.abs((distance - refx) - (points[goingto].x - points[goingfrom].x)) < 5) {
          this.thrusters.left(false);
          this.thrusters.right(false);
          if (goingto == 'home') {
            whatsnext(this);
          } else {
            whatsup = 'inity';
          }
        }
      } else {
        this.radar.angle(180);
      }
      break;
    case 'canigo':
      // see if the doors are open
      if (angle == 90) {
        // nope
        whatsup = 'idunno';
        whatsnext(this);
      } else {
        this.radar.angle(90);
      }
  }
  this.radar.ping();
});

this.on('radar:miss', function(angle) {
  if (angle == 90) {
    // door is open! gogogo
    console.log('damn skippy');
    whatsup = 'gettingthehelloutofdodge';
    this.thrusters.left(false);
    this.thrusters.right(false);
    this.thrusters.bottom(false);
    this.thrusters.top(true);
  } else {
    this.radar.ping();
  }
});