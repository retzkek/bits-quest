/*
 * The round switches won't stay on unless something is placed on top of them.
 */

var stepno=0

/* Steps are defined as an array of arrays, each sub-array contains 
 * three elements:
 * sensor, status of sensor, function to execute
 * The starting condition must be set outside.
 */
var steps = [
  ['sensor:top', true, function(bot) {
    bot.thrusters.bottom(false);
    bot.thrusters.left(true);
  }],
  ['sensor:right', true, function(bot) {
    bot.thrusters.left(false);
    bot.thrusters.top(true);
  }],
  ['sensor:left', true, function(bot) {
    bot.thrusters.top(false);
    bot.thrusters.left(true);
  }],
  ['sensor:right', true, function(bot) {
    bot.thrusters.left(false);
    bot.thrusters.top(true);
  }],
  ['sensor:bottom', true, function(bot) {
    bot.thrusters.top(false);
    bot.thrusters.left(true);
  }],
  ['sensor:right', true, function(bot) {
    bot.thrusters.left(false);
    bot.thrusters.bottom(true);
  }],
  ['sensor:left', true, function(bot) {
    bot.thrusters.bottom(false);
    bot.thrusters.left(true);
  }]];

this.on('start', function() {
  this.thrusters.bottom(true);
  console.log('start');
});
// TODO: remove code repetition
this.on('sensor:top', function(contact) {
  console.log('top '+(contact ? 'on' : 'off'));
  if (steps[stepno][0] == 'sensor:top' && 
      steps[stepno][1] == contact) {
    steps[stepno][2](this);
    if (stepno+1 < steps.length) stepno++;
  }
});
this.on('sensor:right', function(contact) {
  console.log('right '+(contact ? 'on' : 'off'));
  if (steps[stepno][0] == 'sensor:right' && 
      steps[stepno][1] == contact) {
    steps[stepno][2](this);
    if (stepno+1 < steps.length) stepno++;
  }
});
this.on('sensor:bottom', function(contact) {
  console.log('bottom '+(contact ? 'on' : 'off'));
  if (steps[stepno][0] == 'sensor:bottom' && 
      steps[stepno][1] == contact) {
    steps[stepno][2](this);
    if (stepno+1 < steps.length) stepno++;
  }
});
this.on('sensor:left', function(contact) {
  console.log('left '+(contact ? 'on' : 'off'));
  if (steps[stepno][0] == 'sensor:left' && 
      steps[stepno][1] == contact) {
    steps[stepno][2](this);
    if (stepno+1 < steps.length) stepno++;
  }
});
