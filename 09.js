/*
 * Open all three doors to exit.
 *
 * The answer is 5.
 */

this.on('start', function() {
  this.thrusters.left(true);
  this.radar.angle(0);
  this.radar.ping();
});

var step=0;

this.on('radar:hit', function(angle, distance) {
  switch(step) {
    case 0:
      // use radar to line up with first switch on 45 deg
      if (distance < 485) {
        this.thrusters.bottom(true);
        step++;
      }
      break;
    case 2:
      //  kill leftward travel when the bot is vertically aligned with the switch
      //  also set up radar to determine when the bot is below the switch
      if (distance < 185) {
        this.thrusters.left(false);
        this.radar.angle(90);
        step++;
      }
      break;
    case 3:
      // when bot is below the switch, move to line up with exit
      if (distance < 300) {
        this.thrusters.right(true);
        step++;
      }
      break;
  }
  this.radar.ping();
});

this.on('radar:miss', function() {
  if (step == 4) {
    // lined up with exit, gogogo!
    this.thrusters.right(false);
  }
});


this.on('sensor:top', function(contact) {
  if (step == 1 && contact) {
    // move down at 45 to hit third switch (sort of... see radar:hit case 2)
    this.thrusters.bottom(false);
    this.thrusters.top(true);
    step++;
  }
});







