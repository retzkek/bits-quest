/*
 * This other bot is helping-- somewhat.
 */

// start by moving up until we hit the wall 
this.on('start', function() {
  this.thrusters.bottom(true);
});

// now it's a straight shot to the right, just need to 
// make sure our path is clear
this.on('sensor:top', function(collide) {
  if (collide) {
    this.thrusters.bottom(false);
    this.thrusters.left(true);
    this.radar.angle(0);
    this.radar.ping();
  }
});

// if we get close to a wall, post up until it's clear
this.on('radar:hit', function(angle,distance) {
  if (distance < 5) {
    this.thrusters.left(false);
  } else {
    this.thrusters.left(true);
  }
  this.radar.ping();
});

// the way is clear, gogogo!
this.on('radar:miss', function() {
  this.thrusters.left(true);
  this.radar.ping();
});

