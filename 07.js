/*
 * Not only does your robot come equipped with sensors and thrusters. It also
 * has a radar that can be used to determine distances.
 *
 * The radar has two methods:
 *
 *
 *  - angle()       - the current direction the radar is pointing (0-359)
 *  - angle(number) - set the radar direction (0-359)
 *
 *  - ping()        - fires the radar
 *
 * One of two events will return after firing the radar:
 *  - 'radar:hit'   - an object was found
 *  - 'radar:miss'  - no object was found
 *
 * When a hit event is received, the handler will receive the angle the
 * ping was sent out on and the distance to the object, e.g.,
 *    this.on('radar:hit', function(angle, distance) {
 *       // do stuff
 *    });
 *
 *  Bonus info: 
 *
 *      Those red jumpy things will kill your robot. Don't touch them.
 */

var step=0;

this.on('start',function() {
  this.thrusters.left(true);
  this.radar.angle(0);
  this.radar.ping();
});

this.on('radar:hit', function(angle,distance) {
  console.log('hit @ '+angle+' + '+distance);
  if (step == 0 && distance < 280) {
	this.thrusters.left(false);
    this.thrusters.top(true);
    this.radar.angle(90);
    step++;
  } else if (step == 1 && distance < 80) {
    this.thrusters.top(false);
    this.thrusters.left(true);
  }
  this.radar.ping();
});