/*
 * Do it.
 *
 */

// a maze? wall follow! let's go left...

var cango = {
  up: true,
  right: true,
  down: true,
  left: true};

function ohnoes(dir) {
  conole.log("hmm, how did I get here, I should be able to go "+dir);
}

this.on('start', function() {
  this.thrusters.right(true);
});

this.on('sensor:left', function(contact) {
  if (contact) {
    cango.left = false;
    this.thrusters.right(false);
    if (cango.down) {
      this.thrusters.top(true);
    } else  {
      ohnoes("down");
    }
  } else {
    cango.left = true;
    if (this.thrusters.top()) {
      this.thrusters.top(false);
      this.thrusters.right(true);
    }
  }
});

this.on('sensor:right', function(contact) {
  if (contact) {
    cango.right = false;
    this.thrusters.left(false);
    if (cango.up) {
      this.thrusters.bottom(true);
    } else {
      ohnoes("up");
    }
  } else {
    cango.right = true;
    if (this.thrusters.bottom()) {
      this.thrusters.bottom(false);
      this.thrusters.left(true);
    }
  }
});

this.on('sensor:bottom', function(contact) {
  if (contact) {
    cango.down = false;
    this.thrusters.top(false);
    if (cango.right) {
      this.thrusters.left(true);
    } else {
      ohnoes("right");
    }
  } else {
    cango.down = true;
    if (this.thrusters.left()) {
      this.thrusters.left(false);
      this.thrusters.top(true);
    } 
  }
});

this.on('sensor:top', function(contact) {
  if (contact) {
    cango.up = false;
    this.thrusters.bottom(false);
    if (cango.left) {
      this.thrusters.right(true);
    } else {
      ohnoes("left");
    }
  } else {
    cango.up = true;
    if (this.thrusters.right()) {
      this.thrusters.right(false);
      this.thrusters.bottom(true);
    }
  }
});
